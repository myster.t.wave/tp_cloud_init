# TP1 : Programmatic provisioning



## 2. Un peu de conf


🌞 **Ajustez le `Vagrantfile` pour que la VM créée** :

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.network "private_network", ip: "10.1.1.11"

  config.vm.hostname = "ezconf.tp1.efrei"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.customize ["createhd", "--filename", "ezconf.tp1.efrei.vdi", "--size", 20 * 1024] # 20GB
  end
end
```

# II. Initialization script


🌞 **Ajustez le `Vagrantfile`** :

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.network "private_network", ip: "10.1.1.11"

  config.vm.hostname = "ezconf.tp1.efrei"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
  end
  config.vm.provision "shell", inline: "echo 'nameserver 1.1.1.1' | sudo tee /etc/resolv.conf >/dev/null"
  
  config.vm.provision "shell", path: "script.sh"
end
```
script.sh : 
```
dnf update -y

dnf install -y vim python3
```

# III. Repackaging


🌞 **Repackager la VM créée précédemment**

```
Bingo@DESKTOP-DBPDN91 MINGW64 ~/Documents/CloudLeo (master)
$ vagrant package --output rocky-efrei.box
The specified file 'rocky-efrei.box' to save the package as already exists. Please
remove this file or specify a different file name for outputting.

Bingo@DESKTOP-DBPDN91 MINGW64 ~/Documents/CloudLeo (master)
$ vagrant box add rocky-efrei rocky-efrei.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'rocky-efrei' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/Bingo/Documents/CloudLeo/rocky-efrei.box
    box:
==> box: Successfully added box 'rocky-efrei' (v0) for ''!

Bingo@DESKTOP-DBPDN91 MINGW64 ~/Documents/CloudLeo (master)
$ vagrant box list
generic/rocky9 (virtualbox, 4.3.12, (amd64))
rocky-efrei    (virtualbox, 0)

```

# IV. Multi VM

🌞 **Un deuxième `Vagrantfile` qui définit** :

```
  config.vm.box = "generic/rocky9"

  config.vm.define "machine1" do |machine1|
    machine1.vm.network "private_network", ip: "10.1.1.101"
    machine1.vm.hostname = "node1.tp1.efrei"
  end

  config.vm.define "machine2" do |machine2|
    machine2.vm.network "private_network", ip: "10.1.1.102"
    machine2.vm.hostname = "node2.tp1.efrei"
  end

end
```

🌞 **Une fois les VMs allumées, assurez-vous que vous pouvez ping `10.1.1.102` depuis `node1`**

```
$ vagrant ssh machine1
[vagrant@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:cb:b3:b6 brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute eth0
       valid_lft 86319sec preferred_lft 86319sec
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ae:3d:2f brd ff:ff:ff:ff:ff:ff
    altname enp0s8
    inet 10.1.1.101/24 brd 10.1.1.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feae:3d2f/64 scope link
       valid_lft forever preferred_lft forever
[vagrant@node1 ~]$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=0.816 ms
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=0.508 ms
64 bytes from 10.1.1.102: icmp_seq=3 ttl=64 time=0.825 ms
64 bytes from 10.1.1.102: icmp_seq=4 ttl=64 time=1.03 ms
64 bytes from 10.1.1.102: icmp_seq=5 ttl=64 time=0.598 ms
^C
--- 10.1.1.102 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4022ms
rtt min/avg/max/mdev = 0.508/0.756/1.033/0.185 ms

```

# V. cloud-init

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.network "private_network", ip: "10.1.1.11"

  config.vm.hostname = "ezconf.tp1.efrei"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
  end
  config.vm.provision "shell", inline: "echo 'nameserver 1.1.1.1' | sudo tee /etc/resolv.conf >/dev/null"
  
  config.vm.provision "shell", path: "script.sh"
end
```
```
sudo dnf update -y

sudo dnf install -y cloud-init

sudo systemctl enable cloud-init
```

🌞 **Repackager une box Vagrant**


🌞 **Tester !**

```
Vagrant.configure("2") do |config|
  config.vm.box = "rocky_cloud_init.box"

  config.vm.define "cloud-init" do |machine|
    machine.vm.provision "file", source: "user_data.yml", destination: "/tmp/user_data.yml"
    machine.vm.provision "shell", inline: "sudo genisoimage -output /tmp/cloud-init.iso -volid cidata -joliet -rock /tmp/user_data.yml"
    machine.vm.provision "shell", inline: "sudo mv /tmp/cloud-init.iso /vagrant/cloud-init.iso"
  end
end
```